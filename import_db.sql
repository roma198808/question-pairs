CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  body TEXT,
  author_id INTEGER NOT NULL,
  FOREIGN KEY (author_id) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  follower_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (follower_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  subject_question INTEGER NOT NULL,
  parent_id INTEGER,
  author_id INTEGER NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (subject_question) REFERENCES questions(id),
  FOREIGN KEY (parent_id) REFERENCES replies(id),
  FOREIGN KEY (author_id) REFERENCES users(id)
);

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  liker_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (liker_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);


INSERT INTO
  users(fname, lname)
VALUES
  ('Average', 'Joe'), ('Angry', 'Bob');

INSERT INTO
  questions(title, body, author_id)
VALUES
  ('1st question', 'Why does this website suck?', (SELECT id FROM users WHERE fname = 'Angry' AND lname = 'Bob')),
  ('2nd question', 'how use?', (SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe')),
  ('3rd question', 'What is SQL?', (SELECT id FROM users WHERE fname = 'Angry' AND lname = 'Bob'));

INSERT INTO
  replies (subject_question, author_id, body)
VALUES
    ( (SELECT id from questions WHERE title = '1st question'), (SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe'), 'My answer');

INSERT INTO
  question_followers(follower_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe'), (SELECT id from questions WHERE title = '1st question')),
  ((SELECT id FROM users WHERE fname = 'Angry' AND lname = 'Bob'), (SELECT id from questions WHERE title = '1st question')),
  ((SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe'), (SELECT id from questions WHERE title = '2nd question'));

INSERT INTO
  question_likes(liker_id, question_id)
VALUES
  ((SELECT id FROM users WHERE fname = 'Angry' AND lname = 'Bob'), (SELECT id from questions WHERE title = '1st question')),
  ((SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe'), (SELECT id from questions WHERE title = '1st question')),
  ((SELECT id FROM users WHERE fname = 'Average' AND lname = 'Joe'), (SELECT id from questions WHERE title = '2nd question')),
  ((SELECT id FROM users WHERE fname = 'Angry' AND lname = 'Bob'), (SELECT id from questions WHERE title = '1st question'));