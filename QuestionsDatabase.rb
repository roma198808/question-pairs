require 'singleton'
require 'sqlite3'

class QuestionsDatabase < SQLite3::Database

  include Singleton

  def initialize
    super("questions.db")
    self.results_as_hash = true
    self.type_translation = true
  end

end

class User
  attr_accessor :id, :fname, :lname

  def self.find_by_id(id)

    result = QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT *
    FROM users
    WHERE id = (?)
    SQL
    User.new(result[0])
  end

  def initialize(options = {})
    @id = options["id"]
    @fname = options["fname"]
    @lname = options["lname"]
  end

  def save
    if self.id.nil? #create
      params = [self.fname, self.lname]
      QuestionsDatabase.instance.execute(<<-SQL, *params)
        INSERT INTO
          users (fname, lname)
        VALUES
          (?,?)
      SQL
      @id = QuestionsDatabase.instance.last_insert_row_id
    else
      params = [self.fname, self.lname, self.id]
      QuestionsDatabase.instance.execute(<<-SQL, *params)
        UPDATE users
        SET fname = ?, lname = (?)
      WHERE id = (?)
      SQL
    end
  end

  def authored_questions
    Question.find_by_author_id(self.id)
  end

  def authored_replies
    Reply.find_by_user_id(self.id)
  end

  def followed_questions
    QuestionFollower.followed_questions_for_user_id(self.id)
  end

  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end

  def average_karma #needs some further testing
    karma_data = QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT COUNT(CASE WHEN question_likes.question_id IS NULL THEN 0 ELSE question_likes.question_id END)
  / COUNT(DISTINCT questions.id) avg_karma
    FROM questions
    INNER JOIN users
      ON users.id = questions.author_id
    LEFT OUTER JOIN question_likes
      ON questions.id = question_likes.question_id
    WHERE users.id = (?)
    GROUP BY users.id
    SQL
    karma_data.empty? ? 0 : karma_data[0]["avg_karma"]
  end

end

class Question
  attr_accessor :id, :title, :body, :author_id

  def self.find_by_author_id(author_id)

    results = QuestionsDatabase.instance.execute(<<-SQL, author_id)
    SELECT *
    FROM questions
    WHERE author_id = (?)
    SQL
    results.map { |result| Question.new(result) }
  end

  def self.find_by_id(id)
    result = QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT *
    FROM questions
    WHERE id = (?)
    SQL
    Question.new(result[0])
  end

  def self.most_followed(n)
    QuestionFollower.most_followed_questions(n)
  end

  def self.most_liked(n)
    QuestionLike.most_liked_questions(n)
  end

  def initialize(options = {})
    @id = options["id"]
    @title = options["title"]
    @body = options["body"]
    @author_id = options["author_id"]
  end

  def save

    if self.id.nil? #create
      params = [self.title, self.body, self.author_id]
      QuestionsDatabase.instance.execute(<<-SQL, *params)
        INSERT INTO
          questions (title, body, author_id)
        VALUES
          (?,?,?)
      SQL
      @id = QuestionsDatabase.instance.last_insert_row_id
    else
      params = [self.title, self.body, self.author_id, self.id]
      QuestionsDatabase.instance.execute(<<-SQL, *params)
        UPDATE questions
        SET title = (?), body = (?), author_id = (?)
      WHERE id = (?)
      SQL
    end
  end

  def author
    User.find_by_id(self.author_id)
  end

  def replies
    Reply.find_by_question_id(self.id)
  end

  def followers
    QuestionFollower.followers_for_question_id(self.id)
  end

  def num_likes
    QuestionLike.num_likes_for_question_id(self.id)
  end

end

class Reply
  attr_accessor :id, :subject_question, :parent_id, :author_id, :body

  def self.find_by_question_id(subject_question)
    results = QuestionsDatabase.instance.execute(<<-SQL, subject_question)
    SELECT *
    FROM replies
    WHERE subject_question = (?)
    SQL
    results.map { |result| Reply.new(result) }
  end

  def self.find_by_user_id(author_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, author_id)
    SELECT *
    FROM replies
    WHERE author_id = (?)
    SQL
    results.map { |result| Reply.new(result) }
  end

  def self.find_by_id(id)
    result = QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT *
    FROM replies
    WHERE id = (?)
    SQL
    Reply.new(result[0])
  end

  def initialize(options = {})
    @id = options["id"]
    @subject_question = options["subject_question"]
    @parent_id = options["parent_id"]
    @author_id = options["author_id"]
    @body = options["body"]
  end

  def author
    User.find_by_id(self.author_id)
  end

  def question
    Question.find_by_id(subject_question)
  end

  def parent_reply
    parent_id && Reply.find_by_id(self.parent_id)
  end

  def child_replies
    results = QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT *
    FROM replies
    WHERE parent_id = (?)
    SQL
    results.map { |result| Reply.new(result) }
  end

  def likers
    QuestionLike.likers_for_question_id(self.id)
  end

end

class QuestionFollower
  attr_accessor :id, :follower_id, :question_id

  def self.followed_questions_for_user_id(follower_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, follower_id)
    SELECT questions.id, questions.title, questions.body, questions.author_id
    FROM question_followers INNER JOIN questions
    ON question_id = questions.id
    WHERE follower_id = (?)
    SQL
    results.map { |result| Question.new(result) }
  end

  def self.followers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT users.id, users.fname, users.lname
    FROM question_followers INNER JOIN users
    ON follower_id = users.id
    WHERE question_id = (?)
    SQL
    results.map { |result| User.new(result) }
  end

  def self.most_followed_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL, n)
    SELECT questions.id, questions.title, questions.body, questions.author_id,
      COUNT(follower_id)
    FROM question_followers INNER JOIN questions
    ON question_id = questions.id
    GROUP BY question_id
    ORDER BY COUNT(follower_id) DESC
    LIMIT (?)
    SQL
    results.map { |result| Question.new(result) }
  end

  def initialize(options = {})
    @id = options["id"]
    @follower_id = options["follower_id"]
    @question_id = options["question_id"]
  end

end

class QuestionLike
  # attr_accessor :id, :liker_id, :question_id
 def self.likers_for_question_id(question_id)
   results = QuestionsDatabase.instance.execute(<<-SQL, question_id)
   SELECT users.id, users.fname, users.lname
   FROM question_likes INNER JOIN users
   ON liker_id = users.id
   WHERE question_id = (?)
   SQL
   results.map { |result| User.new(result) }
 end

 def self.num_likes_for_question_id(question_id)
   like_data = QuestionsDatabase.instance.execute(<<-SQL, question_id)
   SELECT COUNT(liker_id) num_likes
   FROM question_likes
   GROUP BY question_id
   HAVING question_id = (?)
   SQL
   like_data[0]["num_likes"]
 end

 def self.liked_questions_for_user_id(liker_id)
   results = QuestionsDatabase.instance.execute(<<-SQL, liker_id)
   SELECT questions.id, questions.title, questions.body, questions.author_id
   FROM question_likes INNER JOIN questions
   ON question_id = questions.id
   WHERE liker_id = (?)
   SQL
   results.map { |result| Question.new(result) }
 end

 def self.most_liked_questions(n)
   results = QuestionsDatabase.instance.execute(<<-SQL, n)
   SELECT questions.id, questions.title, questions.body, questions.author_id,
     COUNT(liker_id)
   FROM question_likes INNER JOIN questions
   ON question_id = questions.id
   GROUP BY question_id
   ORDER BY COUNT(liker_id) DESC
   LIMIT (?)
   SQL
   results.map { |result| Question.new(result) }
 end

end